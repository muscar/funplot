extern crate svg;

use svg::Document as SVGDoc;
use svg::node::element::Polygon as SVGPoly;

struct Point {
    x: f64,
    y: f64,
}

struct Polygon {
    a: Point,
    b: Point,
    c: Point,
    d: Point,
}

struct IsoPlotBuilder<F: Fn(f64, f64) -> f64> {
    width: i64,
    height: i64,
    f: F,
    cells: i64,
    xy_range: f64,
    z_scale_factor: f64,
    angle: f64,
}

struct IsoPlot<F: Fn(f64, f64) -> f64> {
    width: i64,
    height: i64,
    f: F,
    cells: i64,
    xy_range: f64,
    xy_scale: f64,
    z_scale: f64,
    angle: f64,
}

struct IsoPlotIter<'a, F: Fn(f64, f64) -> f64 + 'a> {
    plot: &'a IsoPlot<F>,
    x: i64,
    y: i64,
}

impl Into<SVGPoly> for Polygon {
    fn into(self) -> SVGPoly {
        SVGPoly::new().set(
            "points",
            format!(
                "{},{},{},{},{},{},{},{}",
                self.a.x, self.a.y, self.b.x, self.b.y, self.c.x, self.c.y, self.d.x, self.d.y
            ),
        )
    }
}

impl<'a, F: Fn(f64, f64) -> f64 + 'a> IsoPlotBuilder<F> {
    fn new(width: i64, height: i64, f: F) -> Self {
        Self {
            width: width,
            height: height,
            f: f,
            cells: 100,
            xy_range: 30.0,
            z_scale_factor: 0.4,
            angle: std::f64::consts::PI / 6.0,
        }
    }

    fn cells(mut self, cells: i64) -> Self {
        self.cells = cells;
        self
    }

    fn xy_range(mut self, xy_range: f64) -> Self {
        self.xy_range = xy_range;
        self
    }

    fn z_scale_factor(mut self, z_scale_factor: f64) -> Self {
        self.z_scale_factor = z_scale_factor;
        self
    }

    fn angle(mut self, angle: f64) -> Self {
        self.angle = angle;
        self
    }

    fn build(self) -> IsoPlot<F> {
        IsoPlot {
            width: self.width,
            height: self.height,
            f: self.f,
            cells: self.cells,
            xy_range: self.xy_range,
            xy_scale: self.width as f64 / 2.0 / self.xy_range,
            z_scale: self.height as f64 * self.z_scale_factor,
            angle: self.angle,
        }
    }
}

impl<'a, F: Fn(f64, f64) -> f64 + 'a> IsoPlot<F> {
    fn poly(&self, x: i64, y: i64) -> Polygon {
        let (ax, ay) = self.corner(x + 1, y);
        let (bx, by) = self.corner(x, y);
        let (cx, cy) = self.corner(x, y + 1);
        let (dx, dy) = self.corner(x + 1, y + 1);
        Polygon {
            a: Point { x: ax, y: ay },
            b: Point { x: bx, y: by },
            c: Point { x: cx, y: cy },
            d: Point { x: dx, y: dy },
        }
    }

    fn iter(&self) -> IsoPlotIter<F> {
        IsoPlotIter {
            plot: self,
            x: 0,
            y: 0,
        }
    }

    fn corner(&self, i: i64, j: i64) -> (f64, f64) {
        let x = self.xy_range * (i as f64 / self.cells as f64 - 0.5);
        let y = self.xy_range * (j as f64 / self.cells as f64 - 0.5);

        let z = (self.f)(x, y);

        let sx = self.width as f64 / 2.0 + (x - y) * self.angle.cos() * self.xy_scale;
        let sy = self.height as f64 / 2.0 + (x + y) * self.angle.sin() * self.xy_scale
            - z * self.z_scale;

        (sx, sy)
    }
}

impl<'a, F: Fn(f64, f64) -> f64 + 'a> Iterator for IsoPlotIter<'a, F> {
    type Item = Polygon;

    fn next(&mut self) -> Option<Polygon> {
        let p = self.plot.poly(self.x, self.y);
        if self.x >= self.plot.cells {
            return None;
        }
        if self.y < self.plot.cells {
            self.y += 1
        } else {
            self.x += 1;
            self.y = 0
        };
        Some(p)
    }
}

fn f(x: f64, y: f64) -> f64 {
    let r = x.hypot(y);
    r.sin() / r
}

fn main() {
    let plot = IsoPlotBuilder::new(600, 320, &f)
        .cells(100)
        .xy_range(30.0)
        .z_scale_factor(0.4)
        .angle(std::f64::consts::PI / 6.0)
        .build();
    let mut doc = SVGDoc::new()
        .set("fill", "none")
        .set("width", 600)
        .set("height", 320)
        .set("stroke", "grey")
        .set("stroke-width", 0.7);
    for p in plot.iter() {
        doc = doc.add::<SVGPoly>(p.into())
    }
    svg::save("plot.svg", &doc).expect("failed to write SVG");
}
